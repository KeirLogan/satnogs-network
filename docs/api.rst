API
===

SatNOGS Network API is a REST API to get scheduled jobs and post observation data.
This document explains how to use the API to retrieve and post data for your application.


Using API Data
--------------

API access is open to anyone.
All API data are freely distributed under the `CC BY-SA <https://creativecommons.org/licenses/by-sa/4.0/>`_ license.


API Reference
-------------

|api_reference_url|_ contains a full reference of the API.
